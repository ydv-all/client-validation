#!/bin/bash

client1=$1
stage1=$2
client2=$3
stage2=$4

curl --location --request GET "https://rc3jxwm5t6.execute-api.us-east-1.amazonaws.com/dev/v1/deployment/getDeployments?limit=20&client=${client1}&stage=${stage1}" --header 'x-api-key:  DQrZmjJprR4mqmgWJkjfG59h798e0PPxwu2NCxj0' --header 'Content-Type: application/json' | jq '[ .data[] | { deployedBy, repo, commitId, apiName, client, commitTime, source, branch, tag } ]' >data1.json
curl --location --request GET "https://rc3jxwm5t6.execute-api.us-east-1.amazonaws.com/dev/v1/deployment/getDeployments?limit=20&client=${client2}&stage=${stage2}" --header 'x-api-key:  DQrZmjJprR4mqmgWJkjfG59h798e0PPxwu2NCxj0' --header 'Content-Type: application/json' | jq '[ .data[] | { deployedBy, repo, commitId, apiName, client, commitTime, source, branch, tag } ]' >data2.json

rm -rf filter.txt
for i in {0..19}; do
    echo "value of i is $client1 index : ************ : $i"
    k=0
    for j in {0..19}; do
        echo "J is $j"
        if [[ "$(jq .[$i].commitId data1.json)" == "$(jq .[$j].commitId data2.json)" ]]; then
            echo " commit id is same"
            if [[ ("$(jq .[$i].repo data1.json)" =~ "commerce-business-layer") || ("$(jq .[$i].repo data1.json)" =~ "copilot-business-layer") ]]; then
                if [[ "echo $(jq .[$i].apiName data1.json)" == "echo $(jq .[$j].apiName data2.json)" ]]; then
                    echo " Api Name is same "
                    l=1
                    break
                else
                    echo "api not same"
                    m=$j
                    echo "inserted index $m"
                fi
            else
                 break
            fi
        else
            k=$(expr $k + 1)
            if [ $k == 20 ]; then
                echo "*****This Commit id $(jq .[$i].commitId data1.json) is not found in $client2*****" >>filter.txt
                jq "[ .[$i] | { deployedBy, repo, commitId, apiName, client, commitTime, source, branch, tag } ]" data1.json >>filter.txt
            fi
        fi
    done
    if [[ "$(jq .[$i].commitId data1.json)" == "$(jq .[$m].commitId data2.json)" ]]; then
        if [ l!=1 ]; then
            echo "***commit is same***$(jq .[$i].apiName data1.json) is not present on $client2*****" >>filter.txt
            jq "[ .[$i] | { deployedBy, repo, commitId, apiName, client, commitTime, source, branch, tag } ]" data1.json >>filter.txt
        fi
    fi
done
echo -e "\n ***********end of details***********" >>filter.txt
